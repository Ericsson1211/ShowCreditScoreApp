package com.soft.cyber.showcreditscoreapp.connections

import com.soft.cyber.showcreditscoreapp.objects.CreditReport
import retrofit2.Call
import retrofit2.http.GET

interface ApiConnection {

    @get:GET("creditReportInfo.json")
    val creditReportInfor: Call<CreditReport>

    companion object {
        val BASEURL = "https://s3.amazonaws.com/cdn.clearscore.com/native/interview_test/"
    }
}