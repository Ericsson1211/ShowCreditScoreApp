package com.soft.cyber.showcreditscoreapp

import android.content.Context
import android.os.AsyncTask
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.soft.cyber.showcreditscoreapp.connections.ApiConnection
import com.soft.cyber.showcreditscoreapp.objects.CreditReport
import com.soft.cyber.showcreditscoreapp.objects.CreditReportInfo
import kotlinx.android.synthetic.main.activity_dashboard.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class DashboardActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)

        //Calling the Async task for downloading data
        getData(applicationContext).execute()

        //call the methods again when on textview is clicked
        progressBar.setOnClickListener { getData(applicationContext).execute() }
    }

    private fun setTextFields(scoreVal: Int?, max: Int?) {
        if (scoreVal is Int && max is Int) {
            score.text = scoreVal.toString()
            maxScore.text = max.toString()
            setProgressOfDonut(scoreVal / (max / 100))
        }
    }

    private fun setProgressOfDonut(status: Int) {
        progressBar.progress = status
    }


    //Async task for downloading JSON file
    inner class getData(private val context: Context?) : AsyncTask<Void, Void, Boolean>() {


        override fun doInBackground(vararg voids: Void): Boolean? {
            val retrofit = Retrofit.Builder()
                    .baseUrl(ApiConnection.BASEURL)
                    .addConverterFactory(GsonConverterFactory.create()) //Here we are using the GsonConverterFactory to directly convert json data to object
                    .build()

            val apiConnection = retrofit.create(ApiConnection::class.java)
            val creditReportInfoCall = apiConnection.creditReportInfor
            creditReportInfoCall.enqueue(object : Callback<CreditReport> {
                override fun onResponse(call: Call<CreditReport>, response: Response<CreditReport>) {
                    if(response.code() == 200){
                        creditReport = response.body()
                        creditReportInfo = creditReport?.creditReportInfo
                        onPostExecute(true)
                    }
                    else if (context != null){
                        Toast.makeText(context, "Error while trying to connect to Server", Toast.LENGTH_SHORT).show()
                    }
                }

                override fun onFailure(call: Call<CreditReport>, t: Throwable) {
                    Toast.makeText(context, "No internet connection detected", Toast.LENGTH_SHORT).show()
                    t.printStackTrace()
                }
            })
            return null
        }

        override fun onPostExecute(aBoolean: Boolean?) {
            super.onPostExecute(aBoolean)
            if(aBoolean == true) {
                setTextFields(creditReportInfo?.score,creditReportInfo?.maxScoreValue)
            }
        }
    }

    companion object {

        private var creditReport: CreditReport? = CreditReport()
        private var creditReportInfo: CreditReportInfo? = CreditReportInfo()
    }
}